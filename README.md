# Custom Tree Builder
It is web application to manage tree type dataset.<br />
Every node can store numeric value.<br />
Tree is fully editable - add/edit/delete/move.

## Run Instruction
1. Download repository
1. Maven clean install
1. Run EditableTreeApplication.java
1. Change directory to ~/src/main/custom-tree-client
1. npm install / update if needed
1. ng serve
1. Open browser at http://localhost:4200/

## Database
App runs in connection with local database.<br />
You can change DB files location in application.properties.<br />
You have to change spring.datasource.url=jdbc:h2:file:'CUSTOM_FILE_PATH'

## Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.2.5.RELEASE/maven-plugin/)