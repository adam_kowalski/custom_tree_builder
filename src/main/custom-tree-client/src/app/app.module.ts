import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {TreeItemEditComponent} from './tree-item-edit/tree-item-edit.component';
import {SingleTreeLevelComponent} from './tree-item-edit/single-tree-level/single-tree-level.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCardModule} from "@angular/material/card";
import {MatButtonModule} from "@angular/material/button";
import {MatExpansionModule} from "@angular/material/expansion";
import {MatInputModule} from "@angular/material/input";
import { ChangeNodePopupComponent } from './tree-item-edit/single-tree-level/change-node-popup/change-node-popup.component';
import { AddNodePopupComponent } from './tree-item-edit/single-tree-level/add-node-popup/add-node-popup.component';
import {MatDialogModule} from "@angular/material/dialog";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatSelectModule} from "@angular/material/select";
import {HttpClientModule} from "@angular/common/http";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import { MoveNodePopupComponent } from './tree-item-edit/single-tree-level/move-node-popup/move-node-popup.component';

@NgModule({
  entryComponents: [
    ChangeNodePopupComponent,
    AddNodePopupComponent,
    MoveNodePopupComponent
  ],
  declarations: [
    AppComponent,
    TreeItemEditComponent,
    SingleTreeLevelComponent,
    ChangeNodePopupComponent,
    AddNodePopupComponent,
    MoveNodePopupComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FlexLayoutModule,
    MatCardModule,
    MatButtonModule,
    MatExpansionModule,
    MatInputModule,
    MatDialogModule,
    FormsModule,
    MatSelectModule,
    MatToolbarModule,
    ReactiveFormsModule,
    MatSnackBarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
