import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {TreeItemNode} from "../entries/tree-item-node";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class TreeItemServiceService {

  private treeItemUrl: string;

  constructor(private http: HttpClient) {
    this.treeItemUrl = 'http://localhost:8080/tree-item';
  }

  public findAll(): Observable<TreeItemNode[]> {
    return this.http.get<TreeItemNode[]>(this.treeItemUrl)
  }

  public save(treeItem: TreeItemNode) {
    return this.http.post<TreeItemNode>(this.treeItemUrl, treeItem);
  }

  public saveAll(items: TreeItemNode[]) {
    return this.http.post<TreeItemNode>(this.treeItemUrl + '/saveAll', items);
  }

  public changeValue(nodeId: number, newValue: number) {
    let params = new HttpParams()
      .set('nodeId', nodeId.toString())
      .set('newValue', newValue.toString());

    return this.http.post<TreeItemNode>(this.treeItemUrl + '/changeValue', null, {params: params});
  }

  public copy(nodeId: number) {
    return this.http.post<TreeItemNode>(this.treeItemUrl + '/copy', nodeId);
  }

  public delete(nodeId: number) {
    return this.http.post<TreeItemNode>(this.treeItemUrl + '/delete', nodeId);
  }

  public moveNode(nodeFrom: number, nodeTo: number) {
    let params = new HttpParams()
      .set('nodeFrom', nodeFrom.toString())
      .set('nodeTo', nodeTo.toString());

    return this.http.post<TreeItemNode>(this.treeItemUrl + '/moveNode', null, {params: params});
  }
}
