export class TreeItemNode {
  id?: number;
  value?: number;
  parentNode?: TreeItemNode;
  children?: TreeItemNode[];
  sum?: number
}
