import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TreeItemNode} from "../../entries/tree-item-node";
import {MatDialog} from "@angular/material/dialog";
import {ChangeNodePopupComponent} from "./change-node-popup/change-node-popup.component";
import {AddNodePopupComponent} from "./add-node-popup/add-node-popup.component";
import {TreeItemServiceService} from "../../services/tree-item-service.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {MoveNodePopupComponent} from "./move-node-popup/move-node-popup.component";

@Component({
  selector: 'app-single-tree-level',
  templateUrl: './single-tree-level.component.html',
  styleUrls: ['./single-tree-level.component.css']
})
export class SingleTreeLevelComponent implements OnInit {

  @Input() mainNode: TreeItemNode;
  @Input() nodes: TreeItemNode[];
  @Output() reloadTreeEvent = new EventEmitter();

  constructor(public dialog: MatDialog,
              private treeService: TreeItemServiceService,
              private _snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
  }

  changeValue(parentNode: TreeItemNode, $event) {
    $event.stopPropagation();
    const dialogRef = this.dialog.open(ChangeNodePopupComponent, {
      data: parentNode,
      width: '30%'
    });

    this.reloadAfterDialogClose(dialogRef);
  }

  addLeaf(parentNode: TreeItemNode, $event) {
    $event.stopPropagation();
    const dialogRef = this.dialog.open(AddNodePopupComponent, {
      data: parentNode,
      width: '30%'
    });

    this.reloadAfterDialogClose(dialogRef);
  }

  reloadAfterDialogClose(dialogRef) {
    dialogRef.afterClosed().subscribe(value => {
      if (value === true) {
        this.reloadTreeEvent.emit();
        this.openSnackBar('Saved successful', 'Close');
      }
    });
  }

  move(parentNode: TreeItemNode, $event) {
    $event.stopPropagation();
    // getting all nested children of node -> can not move to dependant node
    let allChildrenIds: number[] = [];
    // adding current node and parrent - can not move to self
    allChildrenIds.push(parentNode.id);
    allChildrenIds.push(parentNode.parentNode.id);
    this.getNestedChildren(allChildrenIds, parentNode);
    const allowedNodes = this.nodes.filter(node => !allChildrenIds.includes(node.id));

    const dialogRef = this.dialog.open(MoveNodePopupComponent, {
      data: {editedNode: parentNode, allowedNodes: allowedNodes},
      width: '30%'
    });

    this.reloadAfterDialogClose(dialogRef);
  }

  getNestedChildren(childrenIds: number[], node: TreeItemNode) {
    node.children.forEach(child => {
      childrenIds.push(child.id);
      this.getNestedChildren(childrenIds, child);
    });
  }

  removeLeaf(parentNode: TreeItemNode, $event) {
    $event.stopPropagation();
    const toDelete = this.nodes.find(node => node.id === parentNode.id);
    if (toDelete) {
      this.treeService.delete(toDelete.id).subscribe(() => {
          this.reloadTreeEvent.emit();
          this.openSnackBar('Removed', 'Close');
        },
        error => this.openSnackBar('Error: ' + error, 'Close'));
    }
  }

  copyLeaf(parentNode: TreeItemNode, $event) {
    $event.stopPropagation();
    const toCopy = this.nodes.find(node => node.id === parentNode.id);
    if (toCopy) {
      this.treeService.copy(toCopy.id).subscribe(() => {
          this.reloadTreeEvent.emit();
          this.openSnackBar('Copied', 'Close');
        },
        error => this.openSnackBar('Error: ' + error, 'Close'));
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    })
  }
}
