import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {TreeItemNode} from "../../../entries/tree-item-node";
import {TreeItemServiceService} from "../../../services/tree-item-service.service";
import {FormControl, Validators} from "@angular/forms";

@Component({
  selector: 'app-change-node-popup',
  templateUrl: './change-node-popup.component.html',
  styleUrls: ['./change-node-popup.component.css']
})
export class ChangeNodePopupComponent {

  value: number;
  valueControl: FormControl;

  constructor(public dialogRef: MatDialogRef<ChangeNodePopupComponent>,
              @Inject(MAT_DIALOG_DATA) public data: TreeItemNode,
              private nodeService: TreeItemServiceService) {
    this.value = data.value;
    this.valueControl = new FormControl(this.data.value, [Validators.required, Validators.min(-9999999999), Validators.max(9999999999)])
  }

  save() {
    this.value = this.valueControl.value;
    this.nodeService.changeValue(this.data.id, this.value).subscribe(() => {
      this.dialogRef.close(true);
    })

  }

  close() {
    this.dialogRef.close();
  }

}
