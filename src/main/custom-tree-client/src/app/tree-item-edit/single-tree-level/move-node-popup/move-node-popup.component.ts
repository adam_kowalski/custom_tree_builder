import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {TreeItemNode} from "../../../entries/tree-item-node";
import {TreeItemServiceService} from "../../../services/tree-item-service.service";

@Component({
  selector: 'app-move-node-popup',
  templateUrl: './move-node-popup.component.html',
  styleUrls: ['./move-node-popup.component.css']
})
export class MoveNodePopupComponent {

  selectedNode: TreeItemNode;
  allowedNodes: TreeItemNode[];

  constructor(public dialogRef: MatDialogRef<MoveNodePopupComponent>,
              @Inject(MAT_DIALOG_DATA) public data: MoveNodePopupComponentData,
              private nodeService: TreeItemServiceService) {
    this.allowedNodes = data.allowedNodes;
  }

  moveNode() {
    if (this.selectedNode != null) {
      this.nodeService.moveNode(this.data.editedNode.id, this.selectedNode.id).subscribe(() => {
        this.dialogRef.close(true);
      })
    }
  }

  close() {
    this.dialogRef.close();
  }

}

export class MoveNodePopupComponentData {
  editedNode: TreeItemNode;
  allowedNodes: TreeItemNode[];
}
