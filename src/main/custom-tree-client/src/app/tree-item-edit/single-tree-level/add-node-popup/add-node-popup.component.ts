import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {TreeItemNode} from "../../../entries/tree-item-node";
import {TreeItemServiceService} from "../../../services/tree-item-service.service";
import {FormControl, Validators} from "@angular/forms";

@Component({
  selector: 'app-add-node-popup',
  templateUrl: './add-node-popup.component.html',
  styleUrls: ['./add-node-popup.component.css']
})
export class AddNodePopupComponent {

  newNode: TreeItemNode;
  valueControl: FormControl;

  constructor(public dialogRef: MatDialogRef<AddNodePopupComponent>,
              @Inject(MAT_DIALOG_DATA) public data: TreeItemNode,
              private nodeService: TreeItemServiceService) {
    this.newNode = {parentNode: data, children: []};
    this.valueControl = new FormControl(null, [Validators.required, Validators.min(-9999999999), Validators.max(9999999999)])
  }

  add() {
    this.newNode.value = this.valueControl.value;
    this.nodeService.save(this.newNode).subscribe(() => {
      this.dialogRef.close(true);
    });
  }

  close() {
    this.dialogRef.close();
  }

}
