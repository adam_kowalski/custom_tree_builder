import {Component, OnInit} from '@angular/core';
import {TreeItemNode} from "../entries/tree-item-node";
import {TreeItemServiceService} from "../services/tree-item-service.service";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-tree-item-edit',
  templateUrl: './tree-item-edit.component.html',
  styleUrls: ['./tree-item-edit.component.css']
})
export class TreeItemEditComponent implements OnInit {

  mainNode: TreeItemNode;
  nodes: TreeItemNode[];

  constructor(private treeService: TreeItemServiceService,
              private _snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.treeService.findAll().subscribe(nodes => {
      this.nodes = nodes;
      this.addChildrenToNodes();
      this.calculateSumOnLeaves();
      // searching for mainNode (there can be only one with null parent)
      this.mainNode = this.nodes.filter(node => node.parentNode == null)[0];
    });
  }

  // add children to nodes to avoid cyclic relation
  addChildrenToNodes(): void {
    this.nodes.forEach(node => {
      if (node.children == null) {
        node.children = [];
      }
      if (node.parentNode) {
        const parentNode = this.nodes.find(parent => parent.id === node.parentNode.id);
        if (parentNode.children == null) {
          parentNode.children = []
        }
        parentNode.children.push(node);
      }
    })
  }

  calculateSumOnLeaves(): void {
    // filter for leaves
    const leaves = this.nodes.filter(node => node.children.length === 0);
    leaves.forEach(leaf => {
      // calculate sum from parent nodes recursively
      leaf.sum = this.calculateSum(0, leaf);
    })
  }

  calculateSum(sum: number, node: TreeItemNode) {
    if (node.parentNode) {
      sum += node.parentNode.value;
      return this.calculateSum(sum, node.parentNode);
    }
    return sum;
  }

  saveAll() {
    // clear children -> they are not needed in serialization
    this.nodes.forEach(node => node.children = []);
    this.treeService.saveAll(this.nodes).subscribe(() => {
        this.ngOnInit();
        this.openSnackBar('Saved successful', 'Close');
      },
      error => this.openSnackBar('Error: ' + error, 'Close'));
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    })
  }
}
