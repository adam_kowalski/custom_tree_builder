package com.custom.tree.demo.controllers;

import com.custom.tree.demo.entities.TreeNode;
import com.custom.tree.demo.services.TreeNodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/tree-item")
public class TreeNodeController {

    private final TreeNodeService treeNodeService;

    @Autowired
    public TreeNodeController(TreeNodeService treeNodeService) {
        this.treeNodeService = treeNodeService;
    }

    @GetMapping()
    public List<TreeNode> getNodes() {
        return treeNodeService.getAll();
    }

    @PostMapping()
    void saveTreeNode(@RequestBody TreeNode node) {
        treeNodeService.save(node);
    }

    @PostMapping(value = "/saveAll")
    void saveAll(@RequestBody List<TreeNode> nodes) {
        treeNodeService.save(nodes);
    }

    @PostMapping(value = "/changeValue")
    void saveTreeNode(@RequestParam Long nodeId,
                      @RequestParam BigDecimal newValue) {
        treeNodeService.changeValue(nodeId, newValue);
    }

    @PostMapping(value = "/copy")
    void copy(@RequestBody Long nodeId) {
        treeNodeService.copy(nodeId);
    }

    @PostMapping(value = "/delete")
    void delete(@RequestBody Long nodeId) {
        treeNodeService.delete(nodeId);
    }

    @PostMapping(value = "/moveNode")
    void moveNode(@RequestParam Long nodeFrom,
                      @RequestParam Long nodeTo) {
        treeNodeService.moveNode(nodeFrom, nodeTo);
    }
}
