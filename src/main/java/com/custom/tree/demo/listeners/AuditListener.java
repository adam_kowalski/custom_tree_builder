package com.custom.tree.demo.listeners;

import com.custom.tree.demo.entities.AuditEntity;
import org.springframework.stereotype.Component;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.util.Date;

@Component
public class AuditListener {

    @PrePersist
    public void setInsertDate(AuditEntity entity) {
        entity.setInsertDate(new Date());
    }

    @PreUpdate
    public void setUpdateDate(AuditEntity entity) {
        if (entity.getInsertDate() == null) {
            entity.setInsertDate(new Date());
        }
        entity.setUpdateDate(new Date());
    }
}
