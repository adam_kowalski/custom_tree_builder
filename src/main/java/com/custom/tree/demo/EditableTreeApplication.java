package com.custom.tree.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EditableTreeApplication {

	public static void main(String[] args) {
		SpringApplication.run(EditableTreeApplication.class, args);
	}

}
