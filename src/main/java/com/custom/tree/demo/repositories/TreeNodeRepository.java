package com.custom.tree.demo.repositories;

import com.custom.tree.demo.entities.TreeNode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TreeNodeRepository extends JpaRepository<TreeNode, Long> {

    /**
     * Gets all children by parent ID
     * @param parentId - parent ID
     * @return list of children
     */
    List<TreeNode> getAllByParentNodeId(Long parentId);
}
