package com.custom.tree.demo.entities;

import com.custom.tree.demo.listeners.AuditListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.util.Date;
import java.util.Objects;

@MappedSuperclass
@EntityListeners({AuditListener.class})
public abstract class AuditEntity implements BaseEntity {

    private static final String INSERT_DATE_COL = "INS_DATE";
    private static final String UPDATE_DATE_COL = "UPD_DATE";

    public AuditEntity() {
    }

    public AuditEntity(Long id) {
        this.id = id;
    }

    protected Long id;
    private Date insertDate;
    private Date updateDate;

    @Column(name = INSERT_DATE_COL, nullable = false, updatable = false)
    public Date getInsertDate() {
        return insertDate;
    }

    @Column(name = UPDATE_DATE_COL)
    public Date getUpdateDate() {
        return updateDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, insertDate, updateDate);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final AuditEntity other = (AuditEntity) obj;
        return Objects.equals(this.id, other.id) &&
                Objects.equals(this.insertDate, other.insertDate) &&
                Objects.equals(this.updateDate, other.updateDate);
    }

    public void clearEntityData() {
        this.id = null;
        this.insertDate = null;
        this.updateDate = null;
    }
}
