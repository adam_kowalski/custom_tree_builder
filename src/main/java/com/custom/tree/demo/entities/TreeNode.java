package com.custom.tree.demo.entities;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = TreeNode.TABLE_NAME)
public class TreeNode extends AuditEntity {

    public static final String TABLE_NAME = "TREE_NODES";
    public static final String SEQ_NAME_LC = "tree_nodes_seq";
    public static final String SEQ_NAME_UC = "TREE_NODES_SEQ";

    private BigDecimal value;
    private TreeNode parentNode;

    public TreeNode() {
    }

    public TreeNode(Long id) {
        super(id);
    }

    @Id
    @Override
    @SequenceGenerator(name = SEQ_NAME_LC, sequenceName = SEQ_NAME_UC, allocationSize = 1, initialValue = 1000000)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQ_NAME_LC)
    @Column(name = "ID", nullable = false)
    public Long getId() {
        return id;
    }

    @Column(name = "TREE_VALUE")
    public BigDecimal getValue() {
        return value;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PARENT_NODE")
    public TreeNode getParentNode() {
        return parentNode;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public void setParentNode(TreeNode parentNode) {
        this.parentNode = parentNode;
    }
}
