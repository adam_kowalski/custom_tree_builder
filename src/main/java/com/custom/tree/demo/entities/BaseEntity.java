package com.custom.tree.demo.entities;

import java.io.Serializable;

public interface BaseEntity extends Serializable {

    /**
     * Get ID
     * @return ID
     */
    Long getId();

    static boolean equals(BaseEntity a, BaseEntity b) {
        return (a == null && b == null) ||
                (a != null && b != null) && a.getId() != null && (a.getId().equals(b.getId()));
    }
}
