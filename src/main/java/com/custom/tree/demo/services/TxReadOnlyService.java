package com.custom.tree.demo.services;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.lang.annotation.*;

/**
 * Annotation for read only services
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Transactional(propagation = Propagation.REQUIRED,
        readOnly = true,
        rollbackFor = Exception.class)
public @interface TxReadOnlyService {
}
