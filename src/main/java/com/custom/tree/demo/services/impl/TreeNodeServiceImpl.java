package com.custom.tree.demo.services.impl;

import com.custom.tree.demo.entities.TreeNode;
import com.custom.tree.demo.repositories.TreeNodeRepository;
import com.custom.tree.demo.services.TreeNodeService;
import com.custom.tree.demo.services.TxReadOnlyService;
import com.custom.tree.demo.services.TxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
@TxReadOnlyService
public class TreeNodeServiceImpl extends CrudServiceImpl<TreeNode> implements TreeNodeService {

    private final TreeNodeRepository treeNodeRepository;

    @Autowired
    public TreeNodeServiceImpl(TreeNodeRepository treeNodeRepository) {
        this.treeNodeRepository = treeNodeRepository;
    }

    @Override
    public JpaRepository<TreeNode, Long> getRepository() {
        return treeNodeRepository;
    }

    @Override
    @TxService
    public TreeNode copy(Long nodeId) {
        Optional<TreeNode> treeNode = get(nodeId);
        if (treeNode.isPresent()) {
            TreeNode nodeToCopy = treeNode.get();
            entityManager.detach(nodeToCopy);
            nodeToCopy.clearEntityData();
            TreeNode saved = save(nodeToCopy);
            List<TreeNode> children = treeNodeRepository.getAllByParentNodeId(nodeId);
            children.forEach(child -> {
                child.setParentNode(saved);
                copy(child.getId());
            });
            return saved;
        }
        return null;
    }

    @Override
    @TxService
    public void changeValue(Long nodeId, BigDecimal newValue) {
        Optional<TreeNode> treeNode = get(nodeId);
        if (treeNode.isPresent()) {
            TreeNode nodeToEdit = treeNode.get();
            nodeToEdit.setValue(newValue);
            save(nodeToEdit);
        }
    }

    @Override
    @TxService
    public void moveNode(Long nodeFrom, Long nodeTo) {
        Optional<TreeNode> treeNode = get(nodeFrom);
        if (treeNode.isPresent()) {
            TreeNode toMove = treeNode.get();
            toMove.setParentNode(new TreeNode(nodeTo));
            save(toMove);
        }
    }

    @Override
    public void delete(Long id) {
        List<TreeNode> children = treeNodeRepository.getAllByParentNodeId(id);
        children.forEach(child -> {
            delete(child.getId());
        });
        super.delete(id);
    }
}
