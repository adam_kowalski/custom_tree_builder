package com.custom.tree.demo.services.impl;

import com.custom.tree.demo.services.CrudService;
import com.custom.tree.demo.services.TxReadOnlyService;
import com.custom.tree.demo.services.TxService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
@TxReadOnlyService
public abstract class CrudServiceImpl<T> implements CrudService<T> {

    @PersistenceContext
    protected EntityManager entityManager;

    public abstract JpaRepository<T, Long> getRepository();

    @Override
    public List<T> getAll() {
        return getRepository().findAll();
    }

    @Override
    public Optional<T> get(Long id) {
        return getRepository().findById(id);
    }

    @Override
    @TxService
    public T save(T entity) {
        if (entity == null) {
            return null;
        }
        return getRepository().save(entity);
    }

    @Override
    @TxService
    public List<T> save(Collection<T> entities) {
        if (CollectionUtils.isEmpty(entities)) {
            return new ArrayList<>();
        }
        return getRepository().saveAll(entities);
    }

    @Override
    @TxService
    public void delete(Long id) {
        if (id == null) {
            return;
        }
        get(id).ifPresent(this::delete);

    }

    @Override
    @TxService
    public void delete(T entity) {
        if (entity == null) {
            return;
        }
        getRepository().delete(entity);
    }

    @Override
    @TxService
    public void deleteIds(Collection<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return;
        }
        getRepository().deleteAll(getRepository().findAllById(ids));
    }

    @Override
    @TxService
    public void delete(Collection<T> entites) {
        if (CollectionUtils.isEmpty(entites)) {
            return;
        }
        getRepository().deleteAll(entites);
    }
}
