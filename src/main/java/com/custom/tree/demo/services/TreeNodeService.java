package com.custom.tree.demo.services;

import com.custom.tree.demo.entities.TreeNode;

import java.math.BigDecimal;

/**
 * Service for TreeNode
 */
public interface TreeNodeService extends CrudService<TreeNode> {

    /**
     * Copies tree node with all children
     *
     * @param nodeId - node to copy ID
     * @return copiedNode
     */
    TreeNode copy(Long nodeId);

    /**
     * Changes value of node
     *
     * @param nodeId   node to change ID
     * @param newValue value to set
     */
    void changeValue(Long nodeId, BigDecimal newValue);

    /**
     * Moves node from one to another
     *
     * @param nodeFrom node to move from
     * @param nodeTo   node to move to
     */
    void moveNode(Long nodeFrom, Long nodeTo);
}
