package com.custom.tree.demo.services;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * Universal CRUD service with basic methods
 *
 * @param <T> entity model
 */
public interface CrudService<T> {

    /**
     * Gets all entities from DB
     *
     * @return list of entities
     */
    List<T> getAll();

    /**
     * Gets one entity by ID
     *
     * @param id entity ID
     * @return entity
     */
    Optional<T> get(Long id);

    /**
     * Saves single entity
     *
     * @param entity to save
     * @return saved entity
     */
    T save(T entity);

    /**
     * Saves collection of entities
     *
     * @param entities - collection to save
     * @return saved entities
     */
    List<T> save(Collection<T> entities);

    /**
     * Deletes single entity by ID
     *
     * @param id of entity to delete
     */
    void delete(Long id);

    /**
     * Deletes single entity
     *
     * @param id of entity to delete
     */
    void delete(T id);

    /**
     * Deletes collection of entities
     *
     * @param ids of entities to delete
     */
    void deleteIds(Collection<Long> ids);

    /**
     * Deletes collection of entities
     *
     * @param entities to delete
     */
    void delete(Collection<T> entities);
}
