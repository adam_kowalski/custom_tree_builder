package com.custom.tree.demo.services;

import com.custom.tree.demo.EditableTreeApplication;
import com.custom.tree.demo.entities.TreeNode;
import com.custom.tree.demo.repositories.TreeNodeRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {EditableTreeApplication.class})
@Transactional
public class TreeNodeServiceTest {

    @Autowired
    private TreeNodeService treeNodeService;
    @Autowired
    private TreeNodeRepository treeNodeRepository;

    private TreeNode savedParent;
    private List<TreeNode> savedChildren;

    @Before
    public void initTest() {
        treeNodeRepository.deleteAll();
        TreeNode parentNode = new TreeNode();
        parentNode.setValue(BigDecimal.ZERO);
        TreeNode savedParent = treeNodeService.save(parentNode);
        this.savedParent = savedParent;

        TreeNode child1 = new TreeNode();
        child1.setParentNode(savedParent);
        child1.setValue(BigDecimal.valueOf(1));

        TreeNode child2 = new TreeNode();
        child2.setParentNode(savedParent);
        child2.setValue(BigDecimal.valueOf(2));

        TreeNode child3 = new TreeNode();
        child3.setParentNode(savedParent);
        child3.setValue(BigDecimal.valueOf(3));

        this.savedChildren = treeNodeService.save(Arrays.asList(child1, child2, child3));
    }

    @Test
    public void testGetChildren() {
        List<TreeNode> children = treeNodeRepository.getAllByParentNodeId(this.savedParent.getId());
        Assert.assertEquals(children.size(), 3);
        Assert.assertEquals(this.savedChildren, children);
    }

    @Test
    public void getByIdTest() {
        Optional<TreeNode> treeNode = treeNodeService.get(this.savedParent.getId());
        Assert.assertTrue(treeNode.isPresent());
        Assert.assertEquals(this.savedParent, treeNode.get());
    }

    @Test
    public void getAllTest() {
        List<TreeNode> all = treeNodeService.getAll();
        Assert.assertEquals(4, all.size());
    }

    @Test
    public void saveTreeNodeTest() {
        TreeNode treeNode = new TreeNode();
        treeNode.setValue(BigDecimal.valueOf(150));
        TreeNode saved = treeNodeService.save(treeNode);
        Assert.assertNotNull(saved);
        Optional<TreeNode> found = treeNodeService.get(saved.getId());
        Assert.assertTrue(found.isPresent());
        Assert.assertEquals(saved, found.get());
    }

    @Test
    public void deleteTreeNodeTest() {
        TreeNode child = this.savedChildren.get(0);
        treeNodeService.delete(child.getId());
        Optional<TreeNode> deleted = treeNodeService.get(child.getId());
        // Assert that deleted node does not exist
        Assert.assertFalse(deleted.isPresent());
    }

    @Test
    public void deleteMainTreeNodeWithChildrenTest() {
        treeNodeService.delete(savedParent.getId());
        Optional<TreeNode> deleted = treeNodeService.get(savedParent.getId());
        Assert.assertFalse(deleted.isPresent());
        List<TreeNode> all = treeNodeService.getAll();
        // Assert that there is no nodes after deleting main node (all children deleted with parent)
        Assert.assertEquals(0, all.size());
    }

    @Test
    public void moveNodeTest() {
        TreeNode fromNode = savedChildren.get(0);
        TreeNode toNode = savedChildren.get(1);
        List<TreeNode> beforeMoveChildren = treeNodeRepository.getAllByParentNodeId(toNode.getId());
        // Assert that move to node has no children
        Assert.assertEquals(0, beforeMoveChildren.size());
        treeNodeService.moveNode(fromNode.getId(), toNode.getId());
        List<TreeNode> afterMoveChildren = treeNodeRepository.getAllByParentNodeId(toNode.getId());
        // Assert that move to node has one children after move
        Assert.assertEquals(1, afterMoveChildren.size());
        Optional<TreeNode> one = treeNodeService.get(fromNode.getId());
        // Assert that moved node exists and has toNode as parent
        Assert.assertTrue(one.isPresent());
        Assert.assertEquals(toNode.getId(), one.get().getParentNode().getId());
    }

    @Test
    public void changeNodeTest() {
        BigDecimal valueBeforeChange = savedParent.getValue();
        BigDecimal newValue = BigDecimal.TEN;
        treeNodeService.changeValue(savedParent.getId(), newValue);
        Optional<TreeNode> changed = treeNodeService.get(savedParent.getId());
        // Assert that changed tree node exits
        Assert.assertTrue(changed.isPresent());
        // Assert that value before change is different than new one
        Assert.assertNotEquals(valueBeforeChange, changed.get().getValue());
        // Assert that changed value is equal to new one
        Assert.assertEquals(newValue, changed.get().getValue());
    }

    @Test
    public void getWithWrongIdReturnsEmptyOptional() {
        Optional<TreeNode> treeNode = treeNodeService.get(-1L);
        Assert.assertFalse(treeNode.isPresent());
    }

    @Test
    public void getWithNullThrowsInvalidDataException() {
        Assert.assertThrows(InvalidDataAccessApiUsageException.class, () -> treeNodeService.get(null));
    }

    @Test
    public void saveEmptyListTest() {
        // Assert that saving empty list returns empty list
        Assert.assertEquals(0, treeNodeService.save(new ArrayList<>()).size());
    }

    @Test
    public void copyLeafNodeTest() {
        TreeNode toCopy = savedChildren.get(0);
        // Assert that parent before copy has 3 children
        Assert.assertEquals(3, treeNodeRepository.getAllByParentNodeId(savedParent.getId()).size());
        Long originalId = toCopy.getId();
        TreeNode copied = treeNodeService.copy(originalId);
        // Assert that parent after copy has 4 children
        Assert.assertEquals(4, treeNodeRepository.getAllByParentNodeId(savedParent.getId()).size());
        // Assert that copied nodes has the same value and parent
        Assert.assertEquals(copied.getParentNode(), toCopy.getParentNode());
        Assert.assertEquals(copied.getValue(), toCopy.getValue());
    }

    @Test
    public void copyNodeWithChildren() {
        // Assert that parent before copy has 3 children
        Assert.assertEquals(3, treeNodeRepository.getAllByParentNodeId(savedParent.getId()).size());
        TreeNode copied = treeNodeService.copy(savedParent.getId());
        // Assert that parent after copy has 3 children
        Assert.assertEquals(3, treeNodeRepository.getAllByParentNodeId(savedParent.getId()).size());
        // Assert that copied node has 3 children
        Assert.assertEquals(3, treeNodeRepository.getAllByParentNodeId(copied.getId()).size());
        // Assert that copied nodes has the same value and parent
        Assert.assertEquals(copied.getParentNode(), savedParent.getParentNode());
        Assert.assertEquals(copied.getValue(), savedParent.getValue());
    }

    @Test
    public void copyWithWrongIdReturnsNull() {
        TreeNode copied = treeNodeService.copy(-1L);
        Assert.assertNull(copied);
    }
}
